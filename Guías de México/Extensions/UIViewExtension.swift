//
//  UIViewExtension.swift
//  Guías de México
//
//  Created by Jose Manuel Perez on 10/23/18.
//  Copyright © 2018 home. All rights reserved.
//

import Foundation
import UIKit

extension UIView{
    func setGradientBackground(colorOne: UIColor, colorTwo : UIColor){
        let layer = CAGradientLayer()
        layer.frame = CGRect(x:self.frame.origin.x, y:self.frame.origin.y, width: self.frame.width, height: self.frame.height)
        
        layer.colors = [colorOne.cgColor, colorTwo.cgColor]
        layer.startPoint = CGPoint(x: 0.5, y: 1.0)
        layer.endPoint = CGPoint(x: 0.5, y: 0.0)
        self.layer.addSublayer(layer)
        
//        let gradientLayer = CAGradientLayer()
//
//        gradientLayer.frame = bounds
//        gradientLayer.colors = [colorOne.cgColor, colorTwo.cgColor]
////        gradientLayer.locations = [1.0,1.0]
//        gradientLayer.startPoint = CGPoint(x: 0.5, y: 1.0)
//        gradientLayer.endPoint = CGPoint(x: 0.5, y: 0.0)
//        layer.insertSublayer(gradientLayer, at: 0)
        
    }
    
    
    func addConstraintsWithFormat(format: String, views: UIView...) {
        var viewsDict = [String: UIView]()
        for (index, view) in views.enumerated() {
            view.translatesAutoresizingMaskIntoConstraints = false
            viewsDict["v\(index)"] = view
        }
        addConstraints(NSLayoutConstraint.constraints(withVisualFormat: format, options: NSLayoutConstraint.FormatOptions(), metrics: nil, views: viewsDict))
    }
    
    func addConstraintsFillEntireView(view: UIView) {
        addConstraintsWithFormat(format: "H:|[v0]|", views: view)
        addConstraintsWithFormat(format: "V:|[v0]|", views: view)
    }
    
    func visiblity(gone: Bool, dimension: CGFloat = 0.0, attribute: NSLayoutConstraint.Attribute = .height) -> Void {
        if let constraint = (self.constraints.filter{$0.firstAttribute == attribute}.first) {
            constraint.constant = gone ? 0.0 : dimension
            self.layoutIfNeeded()
            self.isHidden = gone
        }
    }
}
