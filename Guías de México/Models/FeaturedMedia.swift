//
//  FeaturedMedia.swift
//  GuiasMD
//
//  Created by Jose Manuel Perez on 11/6/18.
//  Copyright © 2018 home. All rights reserved.
//
import Foundation

struct MediaResource : Decodable{
    var file : String
    var width : Int
    var height : Int
    var mimeType : String
    var sourceUrl : String
    
    enum CodingKeys : String, CodingKey{
        case file
        case width
        case height
        case mimeType = "mime_type"
        case sourceUrl = "source_url"
    }
}

class FeaturedMedia : Decodable{
    
    var thumbnail : MediaResource
    var medium : MediaResource
//    var large : MediaResource
//    var small : MediaResource
    var full : MediaResource
    var id : Int
    var title : String
    
    enum CodingKeys : String, CodingKey{
        case id
        case title
        case mediaDetails = "media_details"
        case sizes
        case thumbnail
        case medium
//        case large
//        case small
        case full
        case rendered
    }
    
    required init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        self.id = try values.decode(Int.self, forKey: .id)
        let mediaDetailInfo  = try values.nestedContainer(keyedBy: CodingKeys.self, forKey: .mediaDetails)
        let mediaSizesInfo = try mediaDetailInfo.nestedContainer(keyedBy: CodingKeys.self, forKey: .sizes)
        self.thumbnail = try mediaSizesInfo.decode(MediaResource.self, forKey: .thumbnail)
        self.medium = try mediaSizesInfo.decode(MediaResource.self, forKey: .medium)
//        self.large = try mediaSizesInfo.decode(MediaResource.self, forKey: .large)
//        self.small = try mediaSizesInfo.decode(MediaResource.self, forKey: .small)
        self.full = try mediaSizesInfo.decode(MediaResource.self, forKey: .full)
        let titleInfo = try values.nestedContainer(keyedBy: CodingKeys.self, forKey: .title)
        self.title = try titleInfo.decode(String.self, forKey: .rendered)
    }
}

