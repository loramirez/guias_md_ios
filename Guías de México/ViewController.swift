//
//  ViewController.swift
//  Guías de México
//
//  Created by Jose Manuel Perez on 10/23/18.
//  Copyright © 2018 home. All rights reserved.
//

import UIKit
import MaterialComponents

class ViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource {
    
    @IBOutlet weak var heightConstraint: NSLayoutConstraint!
    @IBOutlet weak var collectionViewAvailableGuides: UICollectionView!
    @IBOutlet weak var commingSonHeightConstraint : NSLayoutConstraint!
    @IBOutlet weak var collectionViewCommingSoonGuides: UICollectionView!
    
    
    var guideAvailables : [Guia]  = [Guia]()
    var guidesCommingSoon : [Guia] = [Guia]()
    
    var indexPathArray = [IndexPath]()
    var indexPathArrayCommingSon = [IndexPath]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupNavigationBar()
        self.downloadGuiasAvailables()
        self.downloadGuiasCommingSoon()
        self.collectionViewAvailableGuides.delegate = self
        self.collectionViewAvailableGuides.dataSource = self
        
        self.collectionViewCommingSoonGuides.delegate = self
        self.collectionViewCommingSoonGuides.dataSource = self
        
        let nibCell = UINib(nibName: "PortadaGuiaCell", bundle: nil)
        self.collectionViewAvailableGuides.register(nibCell, forCellWithReuseIdentifier: "cell")
        self.collectionViewCommingSoonGuides.register(nibCell, forCellWithReuseIdentifier: "cell")
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super .viewDidAppear(animated)
        
        let width = (collectionViewAvailableGuides.frame.width - 10) / 2
        let height = (150 * width) / 100;
        let layout = collectionViewAvailableGuides.collectionViewLayout as! UICollectionViewFlowLayout
        layout.itemSize = CGSize(width: width, height: height)
        
        let layout2 = collectionViewCommingSoonGuides.collectionViewLayout as! UICollectionViewFlowLayout
        layout2.itemSize = CGSize(width: width, height: height)
        
        
    }
    
    
    private func setupNavigationBar(){
        let image = UIImage(named: "logo-md")?.imageWithInsets(insets: UIEdgeInsets(top: 40, left: 0, bottom: 40, right: 0))
        let logoImageView = UIImageView(image: image)
        logoImageView.frame = CGRect(x: 0, y: 0, width: 34, height: 34)
        logoImageView.contentMode = .scaleAspectFit
        navigationItem.titleView = logoImageView
    }
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if(collectionView.tag == 1){
            return self.guidesCommingSoon.count
        }
        return self.guideAvailables.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! PortadaGuiaCell
        
        if(collectionView.tag == 0){
            let guia = self.guideAvailables[indexPath.item]
            if !indexPathArray.contains(indexPath) {
                indexPathArray.append(indexPath)
                cell.lblTituloGuia.text = guia.title
                cell.imagePortada.pin_setImage(from: URL(string: guia.featuredMedia.medium.sourceUrl))
                cell.layer.masksToBounds = false;
                cell.setShadowElevation(ShadowElevation(rawValue: 8), for: .normal)
                cell.setShadowColor(UIColor.black, for: .normal)
                cell.cornerRadius = 0.0
            }
        }else {
            let guia = self.guidesCommingSoon[indexPath.item]
            if !indexPathArrayCommingSon.contains(indexPath) {
                indexPathArrayCommingSon.append(indexPath)
                cell.lblTituloGuia.text = guia.title
                cell.imagePortada.pin_setImage(from: URL(string: guia.featuredMedia.medium.sourceUrl))
                cell.layer.masksToBounds = false;
                cell.setShadowElevation(ShadowElevation(rawValue: 8), for: .normal)
                cell.setShadowColor(UIColor.black, for: .normal)
                cell.cornerRadius = 0.0
            }
        }
        return cell;
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if(collectionView.tag == 0){
            let guiaSelected = self.guideAvailables[indexPath.item]
            let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
            let resultViewController = storyBoard.instantiateViewController(withIdentifier: "vc_destinos") as! DestinosViewController
            self.navigationController?.pushViewController(resultViewController, animated: true)
            resultViewController.setGuide(guide: guiaSelected)
        }else{
            let guiaSelected = self.guidesCommingSoon[indexPath.item]
            let popOverVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "fullscreen_comming_soon") as! FullScreenCommingSoonViewController
            self.addChild(popOverVC)
            popOverVC.view.frame = self.view.frame
            self.view.addSubview(popOverVC.view)
            popOverVC.didMove(toParent: self)
           popOverVC.setGuide(guide:guiaSelected)
        }
    }
    
    func downloadGuiasAvailables(){
        let urlGuias = "https://guiasdigitales.wpengine.com/wp-json/wp/v2/pages?&_embed&status=publish&categories=2&orderby=date&order=asc&available=1"
        guard let url = URL(string: urlGuias) else {return}
        
        URLSession.shared.dataTask(with: url) {
            (data,response,err) in
            guard let data = data else { return }
            do{
                self.guideAvailables = try
                    JSONDecoder().decode([Guia].self, from: data)
                DispatchQueue.main.sync {
                    self.collectionViewAvailableGuides.reloadData()
                    
                    let height = self.collectionViewAvailableGuides.collectionViewLayout.collectionViewContentSize.height
                    self.heightConstraint.constant = height
                    self.view.setNeedsLayout()
                    self.view.layoutIfNeeded()
                }
            }catch let jsonErr {
            }
        }.resume()
        
    }
    
    func downloadGuiasCommingSoon(){
        let urlGuias = "https://guiasdigitales.wpengine.com/wp-json/wp/v2/pages?&_embed&status=publish&categories=2&orderby=date&order=asc&available=0"
        
        guard let url = URL(string: urlGuias) else {return}
        
        URLSession.shared.dataTask(with: url) {
            (data,response,err) in
            guard let data = data else { return }
            do{
                self.guidesCommingSoon = try
                    JSONDecoder().decode([Guia].self, from: data)
                DispatchQueue.main.sync {
                    self.collectionViewCommingSoonGuides.reloadData()
                    let height = self.collectionViewCommingSoonGuides.collectionViewLayout.collectionViewContentSize.height
                    self.commingSonHeightConstraint.constant = height
                    self.view.setNeedsLayout()
                    self.view.layoutIfNeeded()
                }
            }catch let jsonErr {
            }
        }.resume()
    }
}

