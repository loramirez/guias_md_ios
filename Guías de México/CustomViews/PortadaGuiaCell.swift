//
//  PortadaGuiaCell.swift
//  Guías de México
//
//  Created by Jose Manuel Perez on 10/23/18.
//  Copyright © 2018 home. All rights reserved.
//

import UIKit
import MaterialComponents

class PortadaGuiaCell: MDCCardCollectionCell {

    @IBOutlet weak var lblTituloGuia: UILabel!
    @IBOutlet weak var imagePortada: UIImageView!
    @IBOutlet weak var viewGradientEffect: UIView!
    @IBOutlet weak var shadowLayer: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        
    }

}
