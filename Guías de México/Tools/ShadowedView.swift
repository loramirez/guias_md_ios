//
//  ShadowedView.swift
//  Guías de México
//
//  Created by Jose Manuel Perez on 10/25/18.
//  Copyright © 2018 home. All rights reserved.
//

import Foundation
import MaterialComponents

class ShadowedView: UIView {
    
    override class var layerClass: AnyClass {
        return MDCShadowLayer.self
    }
    
    var shadowLayer: MDCShadowLayer {
        return self.layer as! MDCShadowLayer
    }
    
    func setDefaultElevation() {
        self.shadowLayer.elevation = .cardResting
    }
    
}
