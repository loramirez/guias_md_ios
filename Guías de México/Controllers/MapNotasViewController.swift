//
//  MapNotasViewController.swift
//  GuiasMD
//
//  Created by Jose Manuel Perez on 11/15/18.
//  Copyright © 2018 home. All rights reserved.
//

import UIKit

import GoogleMaps

protocol MapNotasViewControllerDelegate{
    func onMapReady(controller : MapNotasViewController)
    func onMarkerClicked(notaClicked : Post)
}

class MapNotasViewController: UIViewController, GMSMapViewDelegate {
    
    var mapView : GMSMapView!
    var notas : [Post]!
    var delegate : MapNotasViewControllerDelegate!
    var firstNotaWithCoordinates : Post!

    override func viewDidLoad() {
        super.viewDidLoad()
        let camera = GMSCameraPosition.camera(withLatitude: 19.436351, longitude: -99.1814577, zoom: 8.0)
        self.mapView = GMSMapView.map(withFrame: self.view.bounds, camera: camera)
        self.mapView.delegate = self
        self.view.addSubview(self.mapView)
        
        if((self.delegate) != nil){
            self.delegate.onMapReady(controller: self)
        }
        // Do any additional setup after loading the view.
    }
    
    
    func buildMapa(){
        
        for note in self.notas{
            if((note.ubicacion) != nil){
                if(self.firstNotaWithCoordinates  == nil){
                    self.firstNotaWithCoordinates = note
                }
                
                let marker = GMSMarker()
                let position = CLLocationCoordinate2D(latitude: note.ubicacion.lat, longitude: note.ubicacion.lng)
                var icon : UIImage!
                
                switch(note.categories[0]){
                    case Constants.POST_CATEGORY_ACTIVIDADES:
                        icon = UIImage(named: "pin_actividades")
                        break;
                    case Constants.POST_CATEGORY_COMER:
                        icon = UIImage(named: "pin_comer")
                    case Constants.POST_CATEGORY_DORMIR:
                        icon = UIImage(named: "pin_dormir")
                    case Constants.POST_CATEGORY_ATRACTIVOS:
                        icon = UIImage(named: "pin_atractivos")
                    default:
                       icon = UIImage(named: "pin")
                       break;
                }
                marker.icon = icon
                marker.position = position
                marker.title = note.title
                marker.groundAnchor = CGPoint(x: 0.5, y: 1)
                marker.appearAnimation = GMSMarkerAnimation.pop
                marker.map = self.mapView
                marker.userData = note
              
            }
        }
        self.centerMap()
    }
    
    func centerMap(){
//        if (self.firstNotaWithCoordinates != nil){
//            let coordinates = CLLocationCoordinate2D(latitude: self.firstNotaWithCoordinates.ubicacion.lat, longitude: self.firstNotaWithCoordinates.ubicacion.lng)
//            let pointInPixels = self.mapView.projection.point(for: coordinates)
//            let pixelPointX =  pointInPixels.x
//            let pixelPointY = pointInPixels.y -
//
//        }
    }
    
    func adjustMap(scrollDown : Bool){
        if(self.firstNotaWithCoordinates != nil){
            let cameraPosition = GMSCameraPosition.camera(withLatitude: self.firstNotaWithCoordinates.ubicacion.lat, longitude: self.firstNotaWithCoordinates.ubicacion.lng, zoom: 15.0)
            self.mapView.projection.point(for: <#T##CLLocationCoordinate2D#>)
            
        }
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    
    func mapView(_ mapView: GMSMapView, didTap marker: GMSMarker) -> Bool {
        let note = marker.userData as! Post
        self.delegate.onMarkerClicked(notaClicked: note)
        return true
    }
//    func mapView(_ mapView: GMSMapView, markerInfoWindow marker: GMSMarker) -> UIView? {
//        
//    }
    
//    func mapView(_ mapView: GMSMapView, didTap marker: GMSMarker) -> Bool {
//
//        return true
//    }
}
