//
//  FullScreenCommingSoonViewController.swift
//  GuiasMD
//
//  Created by Jose Manuel Perez on 11/26/18.
//  Copyright © 2018 home. All rights reserved.
//

import UIKit

class FullScreenCommingSoonViewController: UIViewController {

    @IBOutlet weak var image: UIImageView!
    @IBOutlet weak var excerpt: UILabel!
    @IBOutlet weak var lblTitle: UILabel!
    
    var guide : Guia!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    func setGuide(guide : Guia){
        self.guide = guide
        
        if let encodedString = self.guide.featuredMedia.full.sourceUrl.addingPercentEncoding(withAllowedCharacters: .urlFragmentAllowed),
            let url = URL(string: encodedString)
        {
            image.pin_setImage(from: url)
        }
        
        self.excerpt.text = self.guide.excerpt
        self.lblTitle.text = self.guide.title
        
    }
    

    @IBAction func close(_ sender: Any) {
        self.view.removeFromSuperview()
        
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
