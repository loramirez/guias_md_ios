//
//  PostMapViewController.swift
//  GuiasMD
//
//  Created by Jose Manuel Perez on 11/20/18.
//  Copyright © 2018 home. All rights reserved.
//

import UIKit
import GoogleMaps

class PostMapViewController: UIViewController {

    var mapView : GMSMapView!
    var ubicacion : Ubicacion!
    var delegate : PostMapViewControllerDelegate!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let camera = GMSCameraPosition.camera(withLatitude: 19.436351, longitude: -99.1814577, zoom: 8.0)
        self.mapView = GMSMapView.map(withFrame: CGRect.zero, camera: camera)
        self.view = self.mapView
        
        if((self.delegate) != nil){
            self.delegate.onMapReady(controller: self)
        }
    }
    
    
    func buildMapa(){
        if(self.ubicacion != nil){
            let marker = GMSMarker()
            let position = CLLocationCoordinate2D(latitude: ubicacion.lat, longitude: ubicacion.lng)
            marker.position = position
            marker.groundAnchor = CGPoint(x: 0.5, y: 1)
            marker.appearAnimation = GMSMarkerAnimation.pop
            marker.map = self.mapView
            self.mapView.animate(to: GMSCameraPosition.camera(withTarget: position, zoom: 8.0))
        }
    }
}

protocol PostMapViewControllerDelegate {
    func onMapReady(controller : PostMapViewController)
}
